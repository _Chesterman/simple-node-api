var express = require("express");
var app = express();
var fs = require("./userData.json");

app.get("/users", function (req, res) {
  res.json(fs);
});

app.listen(8081, () => {
    console.log('server running on port 8081');
});
